#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
 
class Range {
	private:
		int _top;
		int _bottom;
		int _left;
		int _right;
		
	public:
		Range(int width, int height) :
			_top(0),
			_bottom(height - 1),
			_left(0),
			_right(width - 1)
		{
		}
		
		int top() const { return _top; }
		int bottom() const { return _bottom; }
		int left() const { return _left; }
		int right() const { return _right; }
		
		void top(int y) { _top = y; }
		void bottom(int y) {	_bottom = y;	}
		void left(int x) {	_left = x;	}
		void right(int x) {	_right = x;	}
};


class Batman {
	private:
		int _x;
		int _y;
		
	public:
		Batman(int x, int y) :
			_x(x),
			_y(y)
		{
		}
		
		string deplacer (Range const& range) {
			string pos;
			
			_x = (range.right() - range.left()) / 2 + range.left();
			_y = (range.bottom() - range.top()) / 2 + range.top();

			pos = to_string(_x) + " " + to_string(_y);
			return pos;
		}
		
		void angleVue(Range & range, string direction) {
			for (char dir: direction) {
				if (dir == 'U') { range.bottom(_y - 1); }
				if (dir == 'D') { range.top(_y + 1); }
				if (dir =='L') { range.right(_x - 1);  }
				if (dir =='R') { range.left(_x + 1);  }
			}		
		}
};

int main()
{
    int W; // width of the building.
    int H; // height of the building.
    cin >> W >> H; cin.ignore();
    int N; // maximum number of turns before game over.
    cin >> N; cin.ignore();
    int X0;
    int Y0;
    cin >> X0 >> Y0; cin.ignore();
    
    Batman batman (X0, Y0);
    Range range (W, H);
    
    // game loop
    while (1) {
        string bombDir; // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
        cin >> bombDir; cin.ignore();
        
        batman.angleVue(range, bombDir);
				
		// Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;

        // the location of the next window Batman should jump to.
        cout << batman.deplacer(range) << endl;
    }
}
 
